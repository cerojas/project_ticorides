/*variables*/
const ridesKey='ridesKey';
const usersKey = 'usersKey';
var x = 20;
let userAct;



/*insertar usuario LISTO*/
function insertUser() {
	const firstName = document.getElementById('first_name').value;
	const lastName = document.getElementById('last_name').value;
	const phone = document.getElementById('phone').value;
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;
  const cpassword = document.getElementById('cpassword').value;
	let currentKey = localStorage.getItem('booksLastInsertedId');

	if (!currentKey) {
		localStorage.setItem('booksLastInsertedId', 1);
		currentKey = 1;
	} else {
		currentKey = parseInt(currentKey) + 1;
		localStorage.setItem('booksLastInsertedId', currentKey);
	}



	// create the user object
	const user = {
		firstName,
		lastName,
			phone,
      username,
      password,
      cpassword,
		id: currentKey
	};

  let users = JSON.parse(localStorage.getItem(usersKey));
	if (users && users.length > 0) {
		users.push(user);

	} else {
		users = []
		users.push(user)

	}
	localStorage.setItem(usersKey, JSON.stringify(users));
  alert("USER REGISTERED SUCCESSFULLY ");
	clearFields();
	// render the users
	renderTable('users', users);

}


/*funcion para la creacion de la tabla donde se incluyen los parametros LISTO*/
function renderTable(tableName, tableData) {
	let table = jQuery(`#${tableName}_table`);
	// loop through all the items of table and generates the html
	let rows = "";
	tableData.forEach((user, index) => {
		let row = `<tr><td>${user.first_name}</td><td>${user.last_name}</td><td>${user.phone}</td><td>${user.username}</td><td>${user.password}</td><td>${user.cpassword}</td>`;
		row += `<td> <a onclick="editEntity(this)" data-id="${user.id}" data-entity="${tableName}" class="link edit">Edit</a>  |  <a  onclick="deleteEntity(this);" data-id="${user.id}" data-entity="${tableName}" class="link delete">Delete</a>  </td>`;
		rows += row + '</tr>';
	});
	table.html(rows);
}

function clearFields() {
	document.getElementById('first_name').value = '';
	document.getElementById('last_name').value = '';
	document.getElementById('phone').value = '';
  	document.getElementById('username').value = '';
    	document.getElementById('password').value = '';
      	document.getElementById('cpassword').value = '';
}

/*funcion para obtener el usuario logueado cuando ingreso a las demas pantallas LISTO*/
function usuariologueado () {
var aValue = sessionStorage.getItem('userAct');
document.getElementById('myField').value =aValue;
console.log('userAct',aValue);
}

/*funcion de validar usuario y contraseña para ingresar al dashboard LISTO*/
function Login () {
	debugger;
  let username = document.getElementById('username').value;
  let password = document.getElementById('password').value;
  let listaUsuarios = getFromLocalStorage('usersKey');
  if(listaUsuarios != null){
    for (usua = 0; usua < listaUsuarios.length; usua++){
      if(username == listaUsuarios[usua].username && password == listaUsuarios[usua].password){
				sessionStorage.setItem('userAct', listaUsuarios[usua].username)
				sessionStorage.setItem('idAct', listaUsuarios[usua].id)
              alert("Bienvenido");

               console.log('user', listaUsuarios[usua].username )
							 window.location.href = "Dashboard.html";

            }
			}
				alert("Usuario o Contraseña incorrecto");

	 		}
		}

/*funcion para cargar el nombre de usuario en la pantalla de settings*/
function NombreUsuario () {
		var aValue = sessionStorage.getItem('userAct');
		console.log('userAct',aValue);

		let users = JSON.parse(localStorage.getItem(usersKey));
		let userFound;
		users.forEach(function (user) {
		  if (user.username == aValue) {
		    userFound = user;
		    return;
		  }
		});

		document.getElementById('name_edit').value = userFound.firstName +" "+ userFound.lastName;
}



/*funcion para editar los parametros de usuario*/
function editUser (){

	debugger;

	var aValue = sessionStorage.getItem('userAct');
	var aValue1 = sessionStorage.getItem('idAct');
	var id = parseInt(aValue1);

	let users = JSON.parse(localStorage.getItem(usersKey));
	let userFound;
	users.forEach(function (user) {
	  if (user.id == id) {
	    userFound = user;
	    return;
	  }
	});

const ids= userFound.id;
 const firstName =userFound.firstName;
  const lastName=userFound.lastName;
const phone=	userFound.phone;
	const username=	userFound.username;
		const password=		userFound.password;
					const cpassword=userFound.cpassword;
					const average = document.getElementById('average').value;
					const aboutme = document.getElementById('aboutme').value;


					const user = {
							ids,
						firstName,
						lastName,
						phone,
						username,
						password,
						cpassword,
						average,
						aboutme,

					};

					users = JSON.parse(localStorage.getItem(usersKey));
					let results = users.filter(user => user.id != id);
					results.push(user);
					localStorage.setItem(usersKey, JSON.stringify(results));

				//	clearFields1();
					// render the books
					renderTable('users', users);


	/*let users = JSON.parse(localStorage.getItem(usersKey));
	let results = users.filter(user => user.id == id);
	results.push(user);
	localStorage.setItem(usersKey, JSON.stringify(results));
	renderTable('users', users);*/

}



function loadTableData(tableName) {
	renderTable(tableName, getTableData(tableName));
}

function getTableData(tableName) {
  let tableData = JSON.parse(localStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  return tableData;
}

function getFromLocalStorage(key) {
  return JSON.parse(localStorage.getItem(key));
}



/**
 * Binds the different events to the different elements of the page
 */
function bindEvents() {

	jQuery('#register').bind('click', function (element) {
		insertUser();
	});

  jQuery('#loginbutton').bind('click', function (element) {
    Login();

  });

	jQuery('#Save-settings').bind('click', function (element) {
    editUser();

  });


}
